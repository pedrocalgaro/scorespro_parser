require 'httparty'
require "scorespro_parser/version"
require "scorespro_parser/soccer_match"

module Scorespro
  
  # User and Password from ScoresPro account
  mattr_accessor :user, :password

  def self.setup
    yield self
  end

  class Type
    LIVE          = 1
    TODAY         = 2
    BETWEEN_DATES = 5
    SPORTS        = 11
  end
end
