module Scorespro
  class SoccerMatch
    
    SPORT_ID = 1

    def self.between(initial_date, finish_date)
      options = { 
        state: 'clientUpdate',
        usr: Scorespro.user, 
        pwd: Scorespro.password, 
        type: Scorespro::Type::BETWEEN_DATES,
        start: initial_date,
        :end => finish_date,
        s: SPORT_ID,
        tz: "America/Sao_Paulo"
      }
      
      http_response = JSON.parse(HTTParty.get('http://data2.scorespro.com/exporter/json.php', query: options))
      if http_response["error"]
        result = { error: http_response["error"], matches: {} }
      else 
        matches = http_response["list"]["Sport"]["1"].key?("Matchday") ? http_response["list"]["Sport"]["1"]["Matchday"][Date.today.to_s]["Match"] : {}
        result = { matches: matches}
      end
      
      result
    end

    def self.live(last_update)
      hid = last_update.blank? ? SoccerMatch.get_latest_soccer_hid : last_update
      options = { 
        state: 'clientUpdate',
        usr: Scorespro.user, 
        pwd: Scorespro.password, 
        type: Scorespro::Type::LIVE, 
        s: SPORT_ID,
        h: hid,
        tz: "America/Sao_Paulo"
      }

      http_response = JSON.parse(HTTParty.get('http://data2.scorespro.com/exporter/json.php', query: options))
      if http_response["error"]
        result = { error: http_response["error"], matches: {} }
      else 
        matches = http_response["list"]["Sport"]["1"].key?("Matchday") ? http_response["list"]["Sport"]["1"]["Matchday"][Date.today.to_s]["Match"] : {}
        result = { hid: http_response["list"]["Sport"]["1"]["hid"], matches: matches}
      end

      result
    end

    def self.get_latest_soccer_hid
      options = { 
        state: 'clientStructure',
        type: Scorespro::Type::SPORTS,
      }
      response = JSON.parse(HTTParty.get('http://data2.scorespro.com/exporter/json.php', query: options))
      soccer_last_hid = response["list"]["1"]["hid"]
      
      soccer_last_hid
    end
      
  end
end